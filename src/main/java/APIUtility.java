
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.io.UnsupportedEncodingException;

public class APIUtility {

    public static Response doGet(String query) throws UnsupportedEncodingException {

        RestAssured.baseURI ="https://insights-api.newrelic.com/v1/accounts/737486/query";
        RequestSpecification httpRequest = RestAssured.given();
//                .filter(new ResponseLoggingFilter()).filter(new RequestLoggingFilter());
        httpRequest.accept(ContentType.JSON);
        httpRequest.contentType(ContentType.JSON);
        httpRequest.headers("X-Query-Key","SjbyZ21V_nWIRTIjQLMIS9Yo65EMpkro");
        httpRequest.param("nrql", query);
        Response response = httpRequest.request(Method.GET);
//        String responseBody = response.asString();

//        System.out.println("Response Body is =>  " + responseBody);

        return response;
    }
}


//    select count(*) from MobileRequestError  where statusCode is NOT NULL and  osName ='Android' and (requestPath in ('/api/v1/pop/listing', '/api/v5/listing') or requestPath like '/api/v2/collection%') since 1 day ago facet statusCode, appVersion