
import com.jayway.jsonpath.JsonPath;
import io.restassured.response.Response;
import net.minidev.json.JSONArray;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.type.TypeReference;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class NRQLTest {

    @Test(dataProvider = "dataProvider")
    public void getNRQLData(String page, List<String> apis) throws UnsupportedEncodingException {
        String minutes = System.getenv("MINUTES");

//        String mobileRequestError ="select count(*) from MobileRequestError  where statusCode is NOT NULL and  " +
//                "osName ='Android' and (requestPath in (%s) %s) since 1 day ago facet statusCode, appVersion";
//        String mobileRequest = "select count(*) from MobileRequest  where statusCode is NULL and  osName ='Android' " +
//                "and (requestPath in (%s) %s) since 1 day ago facet statusCode, appVersion";

        String mobileRequestPassAndError = "select count(*) from MobileRequest, MobileRequestError  where statusCode is NOT NULL and  " +
                "osName ='Android' and (requestPath in (%s) %s) since "+minutes+" minute ago facet statusCode";
        String getTotalRequestCount = "select count(*) from MobileRequest, MobileRequestError  where statusCode is NOT NULL and  " +
                "osName ='Android' and (requestPath in (%s) %s) since "+minutes+" minute ago";

//

        StringBuilder inData = new StringBuilder();
        StringBuilder likeData = new StringBuilder();

        for(String api: apis){
            if(api.contains("%")){
                likeData.append("or requestPath like '"+api+"'");
            }else{
                inData.append("'"+api+"',");
            }
        }
        inData.deleteCharAt(inData.length()-1);

        mobileRequestPassAndError = String.format(mobileRequestPassAndError, inData.toString(), likeData.toString());
        getTotalRequestCount = String.format(getTotalRequestCount, inData.toString(), likeData.toString());

        System.out.println("--------------------------------------------");
        System.out.println("Page  :: "+page);
        System.out.println("API's :: "+apis);
        System.out.println("---------------------------------------------");
//        System.out.println("Get total count query :: "+getTotalRequestCount);
        Response response = APIUtility.doGet(getTotalRequestCount);
        Assert.assertFalse(response.getStatusCode()!=200,"Failed while getting total request count");
        Integer totalCount = JsonPath.read(response.asString(),"$.results[0].count");
        System.out.println("Total "+minutes+" minutes ago :: "+ totalCount);
        response = APIUtility.doGet(mobileRequestPassAndError);
        Assert.assertFalse(response.getStatusCode()!=200,"Failed while getting total request count");
        List<HashMap<String,Object>> facets = JsonPath.read(response.asString(),"$.facets");

        System.out.println("Status Code  ::   Count");
        for(HashMap<String,Object> httpCodeCount: facets){
            System.out.println(httpCodeCount.get("name")+"           :   "+((HashMap)((List)httpCodeCount.get("results")).get(0)).get("count"));

        }






    }

    @DataProvider
    public Object[][] dataProvider(){

        File file = new File("./src/main/resources/APIEndPoints");

        String body = null;
        try {
            body = FileUtils.readFileToString(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map = new HashMap<String, Object>();
        mapper.configure(SerializationConfig.Feature.AUTO_DETECT_FIELDS, true);
        try {
            map = mapper.readValue(body, new TypeReference<Map<String, Object>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        Object[][] obj = new Object[map.size()][2];
        int i=0;
        for(Map.Entry<String, Object>apis: map.entrySet()){
            obj[i][0] = apis.getKey();
            obj[i][1] = apis.getValue();
            i++;
        }
        return obj;




    }



}
